from gi.repository import Gtk as gtk
import pyclip

class MainWindow(gtk.Window):

    def __init__(self):
        gtk.Window.__init__(self, title="UList")
        self.set_border_width(10)
        self.set_default_size(700, 400)

        # Boxes
        hbox = gtk.Box(spacing=10)
        hbox.set_homogeneous(False)
        vbox_left = gtk.Box(orientation=gtk.Orientation.VERTICAL, spacing=10)
        vbox_left.set_homogeneous(False)
        vbox_right = gtk.Box(orientation=gtk.Orientation.VERTICAL, spacing=10)
        vbox_right.set_homogeneous(False)

        # Pack the two columns
        hbox.pack_start(vbox_left, True, True, 0)
        hbox.pack_start(vbox_right, True, True, 0)

        # User Text Input
        self.text_input = gtk.TextView()
        scrolled_window = gtk.ScrolledWindow()
        scrolled_window.set_policy(gtk.PolicyType.AUTOMATIC, gtk.PolicyType.AUTOMATIC)
        scrolled_window.add(self.text_input)

        self.textbuffer = self.text_input.get_buffer()
        self.textbuffer.set_text(
            "Insert ..."
        )

        vbox_left.pack_start(scrolled_window, True, True, 0)

        # left align
        listbox = gtk.ListBox()
        listbox.set_selection_mode(gtk.SelectionMode.NONE)

        # Toggle
        row_1 = gtk.ListBoxRow()
        box_1 = gtk.Box()
        row_1.add(box_1)
        label = gtk.Label("Invert Sort                                      ")
        self.check1 = gtk.Switch()
        box_1.pack_start(label, False, False, 0)
        box_1.pack_start(self.check1, False, False, 0)
        listbox.add(row_1)

        row_3 = gtk.ListBoxRow()
        box_3 = gtk.Box()
        row_3.add(box_3)
        label = gtk.Label("Ignore Case                                     ")
        self.check2 = gtk.Switch()
        box_3.pack_start(label, False, False, 0)
        box_3.pack_start(self.check2, False, False, 0)
        listbox.add(row_3)

        row_4 = gtk.ListBoxRow()
        box_4 = gtk.Box()
        row_4.add(box_4)
        label = gtk.Label("Remove Duplicates                          ")
        self.check4 = gtk.Switch()
        box_4.pack_start(label, False, False, 0)
        box_4.pack_start(self.check4, False, False, 0)
        listbox.add(row_4)

        vbox_left.pack_start(listbox, False, False, 0)

        # right align
        label = gtk.Label()
        label.set_text("")
        label.set_justify(gtk.Justification.RIGHT)
        vbox_right.pack_start(label, True, True, 0)

        # Clean Button
        self.button1 = gtk.Button(label="Clean")
        self.button1.connect("clicked", self.button_clicked_clean)

        # Process Button
        self.button2 = gtk.Button(label="Process")
        self.button2.connect("clicked", self.button_clicked_process)

        # Copz Button
        self.button3 = gtk.Button(label="Copy")
        self.button3.connect("clicked", self.button_copy_process)

        vbox_right.pack_start(self.button1, False, False, 0)
        vbox_right.pack_start(self.button2, False, False, 0)
        vbox_right.pack_start(self.button3, False, False, 0)

        self.add(hbox)

    def button_clicked_process(self, widget):
        input_text = self.textbuffer.get_text(self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter(), True)
        output = input_text.split()

        if self.check4.get_active():
            output = set(output)
            output = list(output)

        if self.check2.get_active():
            output = sorted(output, key=str.casefold)
        if self.check2.get_active() & self.check1.get_active():
            output = sorted(output, key=str.casefold, reverse=True)
        elif self.check1.get_active():
            output.sort(reverse=True)
        else:
            output.sort()

        self.textbuffer.set_text('\n'.join(output))

    def button_clicked_clean(self, widget):
        self.textbuffer.set_text("Insert ...")
        self.show_all()

    def button_copy_process(self, widget):
        input_text = self.textbuffer.get_text(self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter(), True)
        pyclip.copy(input_text)
        pyclip.paste(text=True)

window = MainWindow()
window.connect("delete-event", gtk.main_quit)
window.show_all()
gtk.main()