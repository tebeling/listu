# ListU - List Utility Tool #

This is a toy demo example of a list utility tool written as gtk app. The tool allows to sort/inverted sort a list of given items
lexicographically. Additionally, the option to replace items and remove duplicates is given.

# Installation
### Linux
On Linux, this module makes use of the xclip or xsel commands, which should come with the os. 
Otherwise, run "sudo apt-get install xclip" or "sudo apt-get install xsel" (Note: xsel does not always seem to work.)